/*
 * Project        NDS Polaris - AXF
 * (c) copyright  2018
 * Company        HARMAN Automotive Systems GmbH
 *
 *        All rights reserved
 * Secrecy Level  STRICTLY CONFIDENTIAL
 *
 * File           SumWorkTask.java
 * Creation date  2018-11-02
 */

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.RecursiveTask;

public class SumWorkTask extends RecursiveTask<Integer> {

    private Integer num;


    private int threhold;

    public SumWorkTask(Integer num, int threthold) {
        this.num = num;
        this.threhold = threthold;
    }

    @Override
    protected Integer compute() {
        if (num < threhold) {
            SumSubTask sumSubTask = new SumSubTask(0, num + 1);
            sumSubTask.fork();
            return sumSubTask.join();
        }

        int size = num / threhold;


        int start;
        int end;


        List<SumSubTask> tasks = new ArrayList<>();

        for (int i = 0; i < threhold; i++) {

            start = i * size;
            if (i == threhold - 1) {
                end = num;
            } else {
                end = start + size - 1;
            }


            SumSubTask sumSubTask = new SumSubTask(start, end);
            tasks.add(sumSubTask);
            sumSubTask.fork();

        }


        Integer sum = 0;
        for (SumSubTask task : tasks) {
            try {
                sum = sum + task.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        return sum;
    }

}
