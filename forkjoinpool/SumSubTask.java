/*
 * Project        NDS Polaris - AXF
 * (c) copyright  2018
 * Company        HARMAN Automotive Systems GmbH
 *
 *        All rights reserved
 * Secrecy Level  STRICTLY CONFIDENTIAL
 *
 * File           SumTask.java
 * Creation date  2018-11-02
 */

import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class SumSubTask extends RecursiveTask<Integer> {

    static Random random = new Random();

    private Integer start;
    private Integer end;

    public SumSubTask(Integer start, Integer end) {
        this.start = start;
        this.end = end;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ForkJoinPool pool = (ForkJoinPool) Executors.newWorkStealingPool(10);
        SumWorkTask task = new SumWorkTask(1000, 10);

        long start = System.currentTimeMillis();
        pool.submit(task);
        System.out.println("result>" + task.get());
        System.out.println("time:" + ((System.currentTimeMillis() - start) / 1000));

        pool.shutdown();

    }

    @Override
    protected Integer compute() {
        int time = 0;
        try {
            time = random.nextInt(100);
            Thread.sleep(time * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int sum = 0;
        for (int i = start; i < end + 1; i++) {
            sum = sum + i;
        }
        System.out.println(String.format("(%d s)[%s](%d-%d)>%d", time, Thread.currentThread().getName(), start, end, sum));
        return sum;
    }
}
